import React, {Component} from 'react';
import configureStore from './src/store/configureStore'
import {Provider} from 'react-redux'
import {AppRegistry} from 'react-native';
import MainScene from './src/containers/MainScene'

const store = configureStore();

export default class Task6 extends Component {
    render() {
        return (
            <Provider store={store}>
                <MainScene/>
            </Provider>
        );
    }
}

AppRegistry.registerComponent('Task6', () => Task6);
