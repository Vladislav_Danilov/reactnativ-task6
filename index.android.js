import React, {Component} from 'react';
import configureStore from './src/store/configureStore'
import {Provider} from 'react-redux'
import {AppRegistry, Navigator} from 'react-native'
import MainScene from './src/containers/MainScene'
import CredentialScene from './src/containers/CredentialScene'
import DetailsScene from './src/containers/DetailsScene'

const store = configureStore();

export default class Task6 extends Component {

    navigate(route, navigator) {
        if (route.title == 'MainScene') {
            return (
                <Provider store={store}>
                    <MainScene
                        title={route.title}
                        openSetting={()=>{
                            navigator.push({
                                title: 'CredentialScene'
                            });
                        }}
                        openDetails={(data)=>{
                            navigator.push({
                                title: 'DetailsScene',
                                data: data
                            });
                        }}
                    />
                </Provider>
            )
        }
        if (route.title == 'CredentialScene') {
            return (
                <CredentialScene
                    title={route.title}
                    closeSetting={()=>{
                        navigator.pop();
                    }}
                />
            )
        }
        if (route.title == 'DetailsScene') {
            return (
                <DetailsScene
                    title={route.title}
                    data={route.data}
                    closeDetails={()=>{
                        navigator.pop();
                    }}
                />
            )
        }

    }

    render() {
        return (
            <Navigator
                initialRoute={{ title: 'MainScene'}}
                renderScene={(route, navigator) =>{
                    return this.navigate(route, navigator)}
                }
            />
        )
    }
}

AppRegistry.registerComponent('Task6', () => Task6);
