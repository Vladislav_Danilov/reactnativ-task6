import React, {PropTypes, Component} from 'react';
import {AppRegistry, View, StyleSheet, AsyncStorage, NetInfo, Text, TouchableOpacity, Image} from 'react-native';
import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'

import RepositoriesListView from '../components/RepositoriesListView'
import SearcherView from '../components/SearcherView'
import * as updateList from '../actions/UpdateList'
import * as sortRepos from '../actions/SetReposSort'

import {SORT_SETTING} from '../constants/Constants'
import {STYLES} from '../styles/mainStyles'

class MainScene extends Component {

    state = {
        isConnected: null
    };

    constructor(props) {
        super(props);
        const {data} = this.props;
        console.log(this.props);
        const {getRepos} = this.props.updateList;
        this.loadInitialState().done();
        firstReposLoad(getRepos, data.items.length);
    }

    componentDidMount() {
        NetInfo.isConnected.addEventListener(
            'change',
            this.handleConnectivityChange
        );
        NetInfo.isConnected.fetch().done(
            (isConnected) => {
                this.setState({isConnected});
            }
        );
    }

    loadInitialState = async() => {
        try {
            let value = await AsyncStorage.getItem(SORT_SETTING);
            if (value !== null) {
                const {setSort} = this.props.sortRepos;
                setSort(value === 'true');
            } else {
                console.log('Initialized with no selection on disk.');
            }
        } catch (error) {
            console.log('AsyncStorage error: ' + error.message);
        }
    };

    render() {
        const {data} = this.props;
        const {getRepos} = this.props.updateList;
        const {setSort} = this.props.sortRepos;
        if (data.sortAsk) {
            this.onValueChange("true").done();
        } else {
            this.onValueChange("false").done();
        }

        console.log(data);
        if (this.state.isConnected) {
            return (
                <View style={STYLES.mainContainer}>
                    <TouchableOpacity onPress={this.props.openSetting}>
                        <Image style={STYLES.mainSettings} source={require('./../img/settings.png')}/>
                    </TouchableOpacity>
                    <SearcherView getRepos={getRepos}/>
                    <RepositoriesListView
                        sortAsk={data.sortAsk}
                        setSort={setSort}
                        data={data.items}
                        fetching={data.fetching}
                        openDetails={this.props.openDetails}/>
                </View>
            );
        } else {
            return (
                <View style={STYLES.mainConnectView}>
                    <Text>
                        No connect. Please open settings. And enable internet connection.
                    </Text>
                </View>
            );
        }
    }

    componentWillUnmount() {
        NetInfo.isConnected.removeEventListener(
            'change',
            this.handleConnectivityChange
        );
    }

    handleConnectivityChange = (isConnected) => {
        this.setState({
            isConnected,
        });
    };

    onValueChange = async(selectedValue) => {
        try {
            await AsyncStorage.setItem(SORT_SETTING, selectedValue);
            console.log('Saved selection to disk: ' + selectedValue);
        } catch (error) {
            console.log('AsyncStorage error: ' + error.message);
        }
    };
}

function firstReposLoad(getRepos, dataLength) {
    console.log("start load data");
    console.log("dataLength: " + dataLength);
    if (dataLength == 0) {
        getRepos('a');
    } else {
        console.log("data was loaded");
    }
}

function mapStateToProps(state) {
    return {
        data: state.repositories
    }
}

function mapDispatchToProps(dispatch) {
    return {
        updateList: bindActionCreators(updateList, dispatch),
        sortRepos: bindActionCreators(sortRepos, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(MainScene);

AppRegistry.registerComponent('MainScene', () => MainScene);

MainScene.propTypes = {
    openSetting: React.PropTypes.func.isRequired,
    openDetails: React.PropTypes.func.isRequired
};
