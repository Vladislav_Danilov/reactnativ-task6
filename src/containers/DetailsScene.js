import React, {PropTypes, Component} from 'react';
import {AppRegistry, Button, View, StyleSheet, TextInput, Image, AsyncStorage, Text} from 'react-native';

import {STYLES} from '../styles/mainStyles'

export default class DetailsScene extends Component {

    render() {
        return (
            <View style={STYLES.detailContainer}>
                <View style={STYLES.detailSubContainer}>
                    <Image style={STYLES.detailAvatar} source={{ uri: this.props.data.owner.avatar_url}}/>
                </View>
                <View style={STYLES.detailSubContainer}>
                    <Text>
                        Name: {this.props.data.name}
                    </Text>
                </View>
                <View style={STYLES.detailSubContainer}>
                    <Text>
                        Author: {this.props.data.owner.login}
                    </Text>
                </View>
                <View style={STYLES.detailSubContainer}>
                    <Text>
                        Star: {this.props.data.stargazers_count}
                    </Text>
                </View>
                <View style={STYLES.detailSubContainer}>
                    <Text>
                        Description: {this.props.data.description}
                    </Text>
                </View>
                <View style={STYLES.detailButton}>
                    <Button
                        title="Close"
                        color="#841584"
                        onPress={() => {
                            this.props.closeDetails();
                        }}
                    />
                </View>
            </View>
        );
    }
}

AppRegistry.registerComponent('DetailsScene', () => DetailsScene);

DetailsScene.propTypes = {
    closeDetails: React.PropTypes.func.isRequired,
    data: React.PropTypes.object.isRequired
};

