import React, {PropTypes, Component} from 'react';
import {AppRegistry, Button, View, StyleSheet, TextInput, Image, AsyncStorage, Text} from 'react-native';
import {
    CREDENTIAL_URL_SETTING,
    CREDENTIAL_USER_NAME_SETTING
} from '../constants/Constants'
import CredentialView from '../components/CredentialView'

export default class CredentialScene extends Component {

    state = {
        avatarUrl: 'https://assets-cdn.github.com/images/modules/logos_page/Octocat.png',
        userName: 'No name'
    };

    componentDidMount() {
        this.loadInitialState().done();
    }

    render() {
        return (
            <CredentialView credentials={this.state}
                            saveSetting={this.saveCredentials.bind(this)}
                            setAvatarUrl={this.setAvatarUrl.bind(this)}
                            setUserName={this.setUserName.bind(this)}/>
        );
    }

    loadInitialState = async() => {
        try {
            let avatarUrl = await AsyncStorage.getItem(CREDENTIAL_URL_SETTING);
            if (avatarUrl == undefined || avatarUrl == "") {
                avatarUrl = 'https://assets-cdn.github.com/images/modules/logos_page/Octocat.png';
            }
            console.log('Load avatar ' + avatarUrl);
            let userName = await AsyncStorage.getItem(CREDENTIAL_USER_NAME_SETTING);
            console.log('Load name ' + userName);
            if (avatarUrl != null && userName != null) {
                this.setAvatarUrl(avatarUrl);
                this.setUserName(userName)
            } else {
                console.log('Initialized with no selection on disk.');
            }
        } catch (error) {
            console.log('AsyncStorage error: ' + error.message);
        }
    };

    setAvatarUrl(avatarUrl){
        this.setState({ avatarUrl : avatarUrl});
    }

    setUserName(userName){
        this.setState({ userName : userName});
    }

    saveCredentials = async() => {
        try {
            await AsyncStorage.setItem(CREDENTIAL_USER_NAME_SETTING, this.state.userName);
            await AsyncStorage.setItem(CREDENTIAL_URL_SETTING, this.state.avatarUrl);
        } catch (error) {
            console.log('AsyncStorage error: ' + error.message);
        }
    };
}

AppRegistry.registerComponent('CredentialScene', () => CredentialScene);

CredentialScene.propTypes = {
    closeSetting: React.PropTypes.func.isRequired
};
