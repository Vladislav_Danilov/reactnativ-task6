import {
    GET_REPOS_SUCCESS,
    GET_REPOS_FAIL,
    GET_REPOS_REQUEST
} from '../constants/Constants'

export function getRepos(searchText) {
    console.log("start search", searchText);
    return (dispatch) => {
        dispatch({
            type: GET_REPOS_REQUEST,
            payload: []
        });

        fetch('https://api.github.com/search/repositories?q=' + searchText + '+in:name,description&sort=stars&order=desc')
            .then((response) => response.json())
            .then((responseJson) => {
                console.log(dispatch);
                dispatch({
                    type: GET_REPOS_SUCCESS,
                    payload: responseJson.items
                })
            })
            .catch((error) => {
                dispatch({
                    type: GET_REPOS_FAIL,
                    payload: [],
                    error: error
                })
            });
    }
}