import {
    SET_SORT
} from '../constants/Constants'

export function setSort(sortAsk) {
    console.log(sortAsk);
    return {
        type: SET_SORT,
        payload: sortAsk
    }
}
