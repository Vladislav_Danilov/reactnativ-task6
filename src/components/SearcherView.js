import React, {Component} from 'react';
import {AppRegistry, StyleSheet, TextInput, TouchableOpacity, View, Button} from 'react-native';
import {STYLES} from '../styles/mainStyles'

export default class SearcherView extends Component {

    constructor(props) {
        super(props);
        this.state = {text: ''};
    }

    render() {
        let getRepositoriesThrottle = this.throttle(this.props.getRepos, 2000);
        return (
            <View style={STYLES.searchContainer}>
                <View style={STYLES.searchField}>
                    <TextInput
                        placeholder="Input search text"
                        onChangeText={(text) => {
                            this.setState({text});
                            if(text != "" || text != ""){
                                getRepositoriesThrottle(text);
                            }
                        }}
                    />
                </View>
                <Button
                    title="Search"
                    color="#841584"
                    style={STYLES.button}
                    onPress={() => this.props.getRepos(this.state.text.split(' '))}
                />
            </View>
        );
    }

    throttle(func, ms) {

        let isThrottled = false,
            savedArgs,
            savedThis;

        function wrapper() {
            if (isThrottled) {
                savedArgs = arguments;
                savedThis = this;
                return;
            }

            func.apply(this, arguments);

            isThrottled = true;

            setTimeout(function () {
                isThrottled = false;
                if (savedArgs) {
                    wrapper.apply(savedThis, savedArgs);
                    savedArgs = savedThis = null;
                }
            }, ms);
        }

        return wrapper;
    }
}

SearcherView.propTypes = {
    getRepos: React.PropTypes.func.isRequired
};

AppRegistry.registerComponent('SearcherView', () => SearcherView);
