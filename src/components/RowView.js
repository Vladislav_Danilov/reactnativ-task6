import React, {PropTypes, Component} from 'react';
import {View, Text, StyleSheet, Image, Linking, TouchableOpacity, AppRegistry} from 'react-native';
import {STYLES} from '../styles/mainStyles'

export default class Row extends Component {

    state = {
        isHide: false,
    };

    handleHide = (isHide) => {
        this.setState({
            isHide
        });
    };

    render() {
        let data = this.props.data;
        let url = data.html_url;
        if (this.state.isHide) {
            return (
                <View style={STYLES.rowContainer}>
                    <TouchableOpacity onPress={()=>{this.handleHide(false)}}>
                        <Text style={STYLES.rowText}>
                            Result was hided. Tap it to show!
                        </Text>
                    </TouchableOpacity>
                </View>
            );
        } else {
            return (
                <TouchableOpacity onPress={()=> {this.props.openDetails(data)}}>
                    <View style={STYLES.rowContainer}>
                        <View style={STYLES.rowImageContainer}>
                            <Image source={{ uri: data.owner.avatar_url}} style={STYLES.rowPicture}/>
                        </View>
                        <View style={STYLES.rowInfoContainer}>
                            <View style={STYLES.rowContainer}>
                                <View style={STYLES.rowTitleContainer}>
                                    <Text style={STYLES.rowTitle}>
                                        Repo Name: {data.name}
                                    </Text>
                                    <TouchableOpacity onPress={() => {
                                Linking.canOpenURL(url).then(supported => {
                                    if (!supported) {
                                        console.log('Can\'t handle url: ' + url);
                                    } else {
                                        return Linking.openURL(url);
                                    }
                                }).catch(err => console.error('An error occurred', err));
                            }}>
                                        <Image style={STYLES.rowAnchor} source={require('../img/anchor.png')}/>
                                    </TouchableOpacity>
                                </View>
                            </View>
                            <Text style={STYLES.rowText}>
                                User: {data.owner.login}
                            </Text>
                            <View style={STYLES.rowTitleContainer}>
                                <Text style={STYLES.rowText}>
                                    Star: {data.stargazers_count}
                                </Text>
                                <TouchableOpacity onPress={()=>{this.handleHide(true)}}>
                                    <Image style={STYLES.rowEye} source={require('../img/glas.png')}/>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                </TouchableOpacity>
            );
        }
    }
}

Row.propTypes = {
    data: React.PropTypes.object.isRequired,
    openDetails: React.PropTypes.func.isRequired
};

AppRegistry.registerComponent('Row', () => Row);