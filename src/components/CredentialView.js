import React, {PropTypes, Component} from 'react';
import {AppRegistry, Button, View, StyleSheet, TextInput, Image, AsyncStorage, Text} from 'react-native';

import {STYLES} from '../styles/mainStyles'

export default class CredentialView extends Component {

    render() {
        return (
            <View style={STYLES.credentialContainer}>
                <View style={STYLES.credentialSubContainer}>
                    <Image style={STYLES.avatar} source={{uri: this.props.credentials.avatarUrl}}/>
                </View>
                <View style={STYLES.credentialSubContainer}>
                    <TextInput
                        placeholder="Input avatar url"
                        onChangeText={(text) => this.props.setAvatarUrl(text)}
                    />
                </View>
                <View style={STYLES.credentialSubContainer}>
                    <Text style={STYLES.credentialText}>
                        {this.props.credentials.userName}
                    </Text>
                    <TextInput
                        placeholder="Input your name"
                        onChangeText={(text) => this.props.setUserName(text)}
                    />
                </View>
                <View style={STYLES.credentialButton}>
                    <Button
                        title="Save"
                        color="#841584"
                        onPress={() => {
                            this.props.saveSetting().done();
                        }}
                    />
                </View>
            </View>
        );
    }
}

AppRegistry.registerComponent('CredentialView', () => CredentialView);

CredentialView.propTypes = {
    credentials: React.PropTypes.object.isRequired,
    saveSetting: React.PropTypes.func.isRequired,
    setAvatarUrl: React.PropTypes.func.isRequired,
    setUserName: React.PropTypes.func.isRequired
};
