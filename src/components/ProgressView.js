import React, {PropTypes, Component} from 'react';
import {Animated, Easing, AppRegistry, Text, StyleSheet, View} from 'react-native';

import {STYLES} from '../styles/mainStyles'

export default class ProgressView extends Component {

    constructor() {
        super();
        this.spinValue = new Animated.Value(0)
    }

    componentDidMount() {
        this.spin()
    }

    spin() {
        this.spinValue.setValue(0);
        Animated.timing(
            this.spinValue,
            {
                toValue: 1,
                duration: 1000,
                easing: Easing.linear
            }
        ).start(() => this.spin())
    }

    render() {
        const spin = this.spinValue.interpolate({
            inputRange: [0, 1],
            outputRange: ['0deg', '360deg']
        });
        return (
            <View style={STYLES.progressContainer}>
                <Text>
                    Loading...
                </Text>
                <Animated.Image
                    style={{
                        width: 60,
                        height: 60,
                        transform: [{rotate: spin}] }}
                    source={require('../img/progress.png')}/>
            </View>
        );
    }
}

AppRegistry.registerComponent('ProgressView', () => ProgressView);
