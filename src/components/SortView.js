import React, {PropTypes, Component} from 'react';
import {AppRegistry, Text, View, StyleSheet, Switch} from 'react-native';
import {STYLES} from '../styles/mainStyles'

export default class SortView extends Component {
    render() {
        console.log(this.props.sortAsk);
        let setSort = this.props.setSort;
        let sortAsk = this.props.sortAsk;

        return (
            <View style={STYLES.sortContainer}>
                <Switch
                    onValueChange={(value) => {
                        setSort(value);
                        sortAsk = value;
                    }}
                    style={STYLES.sortSwitchStyle}
                    value={this.props.sortAsk}/>
                <Text style={STYLES.sortText}>Sort by star: {this.props.sortAsk ? 'Ask' : 'Desk'}</Text>
            </View>
        );
    }
}

SortView.propTypes = {
    sortAsk: React.PropTypes.bool.isRequired,
    setSort: React.PropTypes.func.isRequired
};

AppRegistry.registerComponent('SortView', () => SortView);
