import React, {PropTypes, Component} from 'react';
import {AppRegistry, ListView, Text, View, StyleSheet} from 'react-native';

import Row from './RowView'
import ProgressView from './ProgressView'
import SortView from '../components/SortView'
import {STYLES} from '../styles/mainStyles'

export default class RepositoriesListView extends Component {

    render() {
        const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
        let data = this.props.data;
        if (this.props.fetching) {
            let dataSource = ds.cloneWithRows(data);
            let setSort = this.props.setSort;
            let sortAsk = this.props.sortAsk;

            return (
                <View style={STYLES.repositoriesListContainer}>
                    <View style={STYLES.repositoriesListSort}>
                        <SortView sortAsk={sortAsk} setSort={setSort}/>
                    </View>
                    <View style={STYLES.repositoriesListView}>
                        <ListView
                            dataSource={dataSource}
                            renderRow={(data) => <Row data={data} openDetails={this.props.openDetails}/>}
                        />
                    </View>
                </View>
            );
        } else {
            return (
                <ProgressView/>
            );
        }
    }
}

RepositoriesListView.propTypes = {
    data: React.PropTypes.array.isRequired,
    fetching: React.PropTypes.bool.isRequired,
    setSort: React.PropTypes.func.isRequired,
    sortAsk: React.PropTypes.bool.isRequired,
    openDetails: React.PropTypes.func.isRequired
};

AppRegistry.registerComponent('RepositoriesListView', () => RepositoriesListView);
