import {StyleSheet} from 'react-native';

export const STYLES = StyleSheet.create({

    searchField: {
        borderRadius: 4,
        borderWidth: 0.5,
        flex: 2,
        flexDirection: 'column',
        justifyContent: 'center',
        height: 30,
        borderColor: '#d6d7da'
    },

    searchContainer: {
        flex: 1,
        margin: 12,
        flexDirection: 'column'
    },

    button: {
        borderRadius: 4,
        borderWidth: 0.5,
        flex: 1,
        margin: 12,
        flexDirection: 'column',
        justifyContent: 'center',
        height: 10,
        alignItems: 'flex-start',
        borderColor: '#d6d7da'
    },

    mainContainer: {
        flex: 5,
        flexDirection: 'column',
        margin: 12
    },

    mainSettings: {
        height: 40,
        width: 40
    },

    mainConnectView: {
        flex: 5,
        flexDirection: 'column',
        margin: 12,
        justifyContent: 'center',
        alignItems: 'center'
    },

    sortContainer: {
        height: 20,
        flexDirection: 'row',
        flex: 1,
        alignItems: 'flex-start',
        marginLeft: 12
    },

    sortSwitchStyle: {
        height: 20,
        flexDirection: 'column',
        marginLeft: 12
    },

    sortText: {
        height: 20,
        flexDirection: 'column',
        marginLeft: 12
    },

    rowContainer: {
        flex: 1,
        padding: 12,
        flexDirection: 'row',
    },

    rowImageContainer: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
    },

    rowInfoContainer: {
        flex: 3,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'flex-start'
    },

    rowTitle: {
        fontSize: 18,
    },

    rowText: {
        marginLeft: 12,
        fontSize: 16,
    },

    rowPicture: {
        height: 40,
        width: 40
    },

    rowAnchor: {
        marginLeft: 5,
        height: 20,
        width: 20
    },

    rowEye: {
        marginLeft: 5,
        height: 40,
        width: 40
    },

    rowTitleContainer: {
        flex: 1,
        flexDirection: 'row'
    },

    repositoriesListContainer: {
        flexDirection: 'column',
        flex: 4,
        justifyContent: 'flex-start',
        marginLeft: 12
    },

    repositoriesListSort: {
        flexDirection: 'column',
        height: 20,
        flex: 1,
        marginLeft: 12
    },

    repositoriesListView: {
        flexDirection: 'column',
        flex: 10,
        marginLeft: 12
    },

    progressContainer: {
        flexDirection: 'column',
        flex: 4,
        marginLeft: 12
    },

    credentialContainer: {
        flexDirection: 'column',
        flex: 4,
        justifyContent: 'flex-start',
        marginLeft: 12
    },

    credentialAvatar: {
        margin: 20,
        height: 80,
        width: 80,
        borderWidth: 5
    },

    credentialSubContainer: {
        flex: 1,
        flexDirection: 'column'
    },

    credentialButton: {
        flex: 1,
        flexDirection: 'column',
        height: 40,
        width: 70,
        marginBottom: 20
    },

    credentialText: {
        fontSize: 24
    },

    detailContainer: {
        flexDirection: 'column',
        flex: 4,
        justifyContent: 'flex-start',
        marginLeft: 12
    },

    detailAvatar: {
        flexDirection: 'column',
        margin: 20,
        height: 50,
        width: 50,
        borderWidth: 5
    },

    detailSubContainer: {
        flex: 1,
        flexDirection: 'row'
    },

    detailButton: {
        flex: 1,
        flexDirection: 'column',
        height: 40,
        width: 70,
        marginBottom: 20
    }
});