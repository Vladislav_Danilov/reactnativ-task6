import {
    GET_REPOS_SUCCESS,
    GET_REPOS_FAIL,
    GET_REPOS_REQUEST,
    SET_SORT
} from '../constants/Constants'

const initialState = {
    items: [],
    fetching: false,
    sortAsk: false
};

export default function repositories(state, action) {
    console.log(action);
    switch (action.type) {
        case GET_REPOS_SUCCESS:
            console.log("update state: ");
            if (action.payload != undefined) {
                return {...state, items: action.payload, fetching: true, sortAsk: state.sortAsk};
            } else {
                return state;
            }
        case GET_REPOS_REQUEST:
            return {...state, items: [], fetching: false, sortAsk: state.sortAsk};
        case SET_SORT:
            state.items.sort(function (a, b) {
                if (a.stargazers_count > b.stargazers_count) {
                    if (action.payload) {
                        return 1;
                    } else {
                        return -1;
                    }
                }
                if (a.stargazers_count < b.stargazers_count) {
                    if (action.payload) {
                        return -1;
                    } else {
                        return 1;
                    }
                }
                return 0;
            });
            return {...state, items: state.items, fetching: state.fetching, sortAsk: action.payload};
        default:
            return initialState;
    }
}
